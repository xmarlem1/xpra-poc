# README -- jnlp app in a docker container

JNLP sample taken from:
https://blog.sebastian-daschner.com/entries/java_web_start_in_docker_sandbox


## Build

```bash
docker build -t jnlpapp .
```

## Run with docker

```bash 
docker run -it \
 --name jnlp-sample \
 --volumes-from x11-bridge \
 --rm \
 -v $(pwd):/jnlp \
 -e DISPLAY=:14 \
 jnlpapp /jnlp/dynamictree_webstart.jnlp
```
