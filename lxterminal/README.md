# LxTerminal example

## Build

```bash
docker build -t lxterminal:latest .
```

### Build alpine version (light weight)

```bash
docker build -f Dockerfile.alpine -t lxterminal:alpine
```


## Start with docker

```bash
docker run -d \
 --name lxterminal \
 --volumes-from x11-bridge \
 -e DISPLAY=:14 \
 lxterminal
```
