# XPra (multi-platform screen and application forwarding system "screen for X11") running in containers ---> POC

## Manual Steps:

- First start the container with xpra server

```bash
docker run -d \
 --name x11-bridge \
 -e MODE="tcp" \
 -e XPRA_HTML="yes" \
 -e DISPLAY=:14 \
 -e XPRA_PASSWORD=111 \
 -p 10000:10000 \
 jare/x11-bridge
```


- then connect to the xpra console webpage:

http://localhost:10000/index.html?encoding=rgb32&password=111

(with no other containers, this browser windows looks empty)


- now you can start a container with a gui app running inside.. .eg...lxterminal

```bash
docker run -d \
 --name lxterminal \
 --volumes-from x11-bridge \
 -e DISPLAY=:14 \
 lxterminal
```

*(first you have to build this image from lxterminal proj folder --> check lxterminal README file)*

NB. I have also create a jnlp example, with related Dockerfile and README in "jnlp_test" folder.
Once done the build, you can always start the container (manually) with:

```bash
docker run -d \
 --name jnlp-sample \
 --volumes-from x11-bridge \
 --rm \
 -v $(pwd):/jnlp \
 -e DISPLAY=:14 \
 jnlpapp /jnlp/dynamictree_webstart.jnlp
```

## TED

```bash
./start-ted   # will start x11_bridge and after few seconds the target app (lxterminal)
```

```bash
./stop-ted   # will stop all containers
```
